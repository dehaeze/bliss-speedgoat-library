call C:\ProgramData\Miniconda3\Scripts\activate.bat C:\ProgramData\Miniconda3
call activate bliss_env

set speedgoatname=%1

if %speedgoatname%==speedgoat-nass-1    (call bliss-speedgoat-server 172.24.163.176:22224 --port 8202)
if %speedgoatname%==speedgoat-mel-1     (call bliss-speedgoat-server 172.24.163.178:22226 --port 8203)
if %speedgoatname%==speedgoat-mel-2     (call bliss-speedgoat-server 172.24.163.181:22225 --port 8205)
if %speedgoatname%==speedgoat-mel-edu-1 (call bliss-speedgoat-server 172.24.163.180:22223 --port 8204)
if %speedgoatname%==speedgoat-id21-1    (call bliss-speedgoat-server 160.103.41.187:22222 --port 8210)
if %speedgoatname%==speedgoat-id24-1    (call bliss-speedgoat-server 160.103.44.194:22222 --port 8211)
if %speedgoatname%==speedgoat-bm23-1    (call bliss-speedgoat-server 172.24.163.175:22222 --port 8201)
if %speedgoatname%==TargetPC1           (call bliss-speedgoat-server 192.168.7.1:22222 --port 8201)
