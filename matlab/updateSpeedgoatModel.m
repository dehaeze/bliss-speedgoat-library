function [] = updateSpeedgoatModel(mdl)

%% Set all scopes IDs properly
bliss_scopes = find_system(mdl,'LookUnderMasks','on','FollowLinks', 'on', 'name', 'bliss_scope');
bliss_ring_buffers = find_system(mdl,'LookUnderMasks','on','FollowLinks', 'on', 'name', 'bliss_ringbuffer');
bliss_fast_daq = find_system(mdl,'LookUnderMasks','on','FollowLinks', 'on', 'name', 'bliss_fastdaq');

for i = 1:length(bliss_scopes)
    set_param(bliss_scopes{i}(1:end-12), 'Commented', 'on')
end
for i = 1:length(bliss_ring_buffers)
    set_param(bliss_ring_buffers{i}(1:end-17), 'Commented', 'on')
end
for i = 1:length(bliss_fast_daq)
    set_param(bliss_fast_daq{i}(1:end-14), 'Commented', 'on')
end

for i = 1:length(bliss_scopes)
    set_param(bliss_scopes{i}(1:end-12), 'Commented', 'off')
    set_param([bliss_scopes{i}(1:end-12) '/scope'], 'scopeno', get_param([bliss_scopes{i}(1:end-12) '/scope_id'], 'Value'))
end
for i = 1:length(bliss_ring_buffers)
    set_param(bliss_ring_buffers{i}(1:end-17), 'Commented', 'off')
    set_param([bliss_ring_buffers{i}(1:end-17) '/scope'], 'scopeno', get_param([bliss_ring_buffers{i}(1:end-17) '/scope_id'], 'Value'))
end
for i = 1:length(bliss_fast_daq)
    set_param(bliss_fast_daq{i}(1:end-14), 'Commented', 'off')
    set_param([bliss_fast_daq{i}(1:end-14) '/scope'], 'scopeno', get_param([bliss_fast_daq{i}(1:end-14) '/scope_id'], 'Value'))
end

%% Update Model
set_param(mdl,'SimulationCommand','Update')

end
