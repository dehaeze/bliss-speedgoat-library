%% Sampling Time
Ts_ = 1e-4; % Sampling Time [s]

%% Feedback Control
K = tf(1, [1/2/pi 0]);
[regul_num_, regul_den_] = tfdata(c2d(K, Ts_, 'tustin'));
regul_num_ = regul_num_{1};
regul_den_ = regul_den_{1};
clear K;

%% Counters
counters_ = [
    struct('name', 'sig_test', 'unit', 'V', 'description', ''), ...
    struct('name', 'motor_pos', 'unit', 'nm', 'description', ''), ...
    struct('name', 'pos_error', 'unit', 'nm', 'description', ''), ...
    struct('name', 'ctrl_status', 'unit', 'bool', 'description', ''), ...
    struct('name', 'plant_output', 'unit', 'nm', 'description', '') ...
];

filtered_counters_ = {'plant_output', 'pos_error'};
for counter_name_ = filtered_counters_
    filtered_counter_ = counters_(ismember({counters_.name}, counter_name_));
    filtered_counter_.name = strcat(filtered_counter_.name, '_filtered');
    filtered_counter_.description = strcat(filtered_counter_.description, ' - Filtered');
    counters_ = [counters_, filtered_counter_];
end

%% Test Plant
G = tf(1,[1/2/pi/100 1]);
[plant_num_, plant_den_] = tfdata(c2d(G, Ts_, 'tustin'));
plant_num_ = plant_num_{1};
plant_den_ = plant_den_{1};
clear G;
